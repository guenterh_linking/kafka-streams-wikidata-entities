/*
 * Copyright (C) 2019  Project swissbib <https://swissbib.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package org.swissbib.linked

import com.beust.klaxon.Json
import com.beust.klaxon.TypeAdapter
import com.beust.klaxon.TypeFor
import java.lang.IllegalArgumentException
import java.time.OffsetDateTime
import kotlin.reflect.KClass

data class LanguageField(
    val language: String,
    val value: String
)

data class SiteLink(
    val site: String,
    val title: String,
    val badges: List<String>,
    val url: String? = null
)


@TypeFor(field = "type", adapter = DataValueTypeAdapter::class)
open class DataValue(
    val type: String
)

data class StringValue(
    val value: String
) : DataValue("string")

data class WikibaseEntityId(
    val value: WikibaseEntityIdValue
): DataValue("wikibase-entityid")

data class WikibaseEntityIdValue(
    @Json(name = "entity-type")
    val entityType: String,
    @Json(name = "numeric-id")
    val numericId: Long
)

data class GlobeCoordinateValue(
    val value: GlobeCoordinate
) : DataValue("globecoordinate")

data class GlobeCoordinate(
    val latitude: Float,
    val longitude: Float,
    // is deprecated
    val altitude: Int? = null,
    val precision: Float,
    val globe: String
)

data class QuantityValue(
    val value: Quantity
) : DataValue("quantity")

data class Quantity(
    val amount: String,
    val upperBound: String,
    val lowerBound: String,
    val unit: String
)

data class TimeValue(
    val value: Time
) : DataValue("time")

data class Time(
    val time: String,
    @Json(name = "timezone")
    val timeZone: Int,
    @Json(name = "calendarmodel")
    val calendarModel: String,
    val precision: Int,
    // both before and after are unsused and may be dropped in the future.
    val before: Int?,
    val after: Int?
)

class DataValueTypeAdapter : TypeAdapter<DataValue> {
    override fun classFor(type: Any): KClass<out DataValue> = when(type as String){
        "string" -> StringValue::class
        "wikibase-entityid" -> WikibaseEntityId::class
        "globecoordinate" -> GlobeCoordinateValue::class
        "quantity" -> QuantityValue::class
        "time" -> TimeValue::class
        else -> throw IllegalArgumentException("Could not parse type $type in wikidata snak.")
    }
}

data class Snak(
    val hash: String? = null,
    @Json(name = "snaktype")
    val snakType: String,
    val property: String,
    val datatype: String,
    @Json(name = "datavalue")
    val dataValue: DataValue
)

data class Reference(
    val hash: String,
    val snaks: HashMap<String, List<Snak>>,
    @Json(name = "snaks-order")
    val snaksOrder: List<String>
)

data class Claim(
    val id: String,
    @Json(name = "mainsnak")
    val mainSnak: Snak? = null,
    val type: String? = null,
    val rank: String? = null,
    val qualifiers: HashMap<String, List<Snak>>? = null,
    @Json("qualifiers-order")
    val qualifiersOrder: List<String>? = null,
    val references: List<Reference>? = null
)

data class WikidataEntity(
    val id: String,
    val title: String? = null,
    val type: String,
    @Json(name = "pageid")
    val pageId: Int?,
    @Json(name = "ns")
    val namespace: Int? = null,
    val labels: HashMap<String, LanguageField>,
    val descriptions: HashMap<String, LanguageField>,
    val aliases: HashMap<String, LanguageField>,
    val claims: HashMap<String, List<Claim>>,
    @Json(name = "sitelinks")
    val siteLinks: HashMap<String, SiteLink>,
    @Json(name = "lastrevid")
    val lastRevId: Long,
    val modified: OffsetDateTime
)