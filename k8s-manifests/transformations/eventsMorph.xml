<?xml version="1.0" encoding="UTF-8"?>

<!--
  ~ Copyright (C) 2019  Project swissbib <https://swissbib.org>
  ~
  ~ This program is free software: you can redistribute it and/or modify
  ~ it under the terms of the GNU Affero General Public License as
  ~ published by the Free Software Foundation, either version 3 of the
  ~ License, or (at your option) any later version.
  ~
  ~ This program is distributed in the hope that it will be useful,
  ~ but WITHOUT ANY WARRANTY; without even the implied warranty of
  ~ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  ~ GNU Affero General Public License for more details.
  ~
  ~ You should have received a copy of the GNU Affero General Public License
  ~ along with this program.  If not, see <https://www.gnu.org/licenses/>.
  -->

<metamorph xmlns="http://www.culturegraph.org/metamorph" version="1">

    <macros>
        <macro name="language-tag">
            <entity name="$[abbrev]" flushWith="$[predicate]" reset="true">
                <combine name="${lang}" value="${value}" flushWith="$[predicate]">
                    <data name="value" source="$[predicate]">
                        <replace pattern='##@.+' with=''/>
                    </data>
                    <data name="lang" source="$[predicate]">
                        <regexp match='##@([^ ]+)' format="${1}"/>
                    </data>
                </combine>
            </entity>
        </macro>
        <macro name="filter-language-tag">
            <combine name="$[tempName]" value="${value}" flushWith="$[predicate]" reset="true">
                <if>
                    <data source="$[predicate]">
                        <split delimiter="##"/>
                        <occurrence only="moreThan 1"/>
                        <whitelist>
                            <entry name="@en"/>
                            <entry name="@en-gb"/>
                            <entry name="@en-ca"/>
                            <entry name="@de"/>
                            <entry name="@de-ch"/>
                            <entry name="@de-at"/>
                            <entry name="@it"/>
                            <entry name="@fr"/>
                        </whitelist>
                    </data>
                </if>
                <data source="$[predicate]" name="value"/>
            </combine>
        </macro>
        <macro name="remove-datatype">
            <data source="$[predicate]" name="$[abbrev]">
                <split delimiter="##"/>
                <occurrence only="lessThan 2"/>
            </data>
        </macro>
    </macros>

    <rules>

        <!-- currently only includes predicates with at least 2000+ occurrences -->
        <data source="_id" name="\@id"/>

        <data source="http://www.w3.org/1999/02/22-rdf-syntax-ns#type" name="\@type"/>

        <!-- instance of should only be present once. -->
        <data source="http://www.w3.org/1999/02/22-rdf-syntax-ns#type" name="\@context">
            <constant value="https://resources.swissbib.ch/wikidata/item/context.jsonld"/>
        </data>

        <!-- Wikidata -->
        <!-- country -->
        <data source="http://www.wikidata.org/prop/direct/P17" name="wdt:P17"/>
        <!-- image -->
        <data source="http://www.wikidata.org/prop/direct/P18" name="wdt:P18"/>
        <!-- instance of -->
        <data source="http://www.wikidata.org/prop/direct/P31" name="wdt:P31"/>
        <!-- follows (other event) -->
        <data source="http://www.wikidata.org/prop/direct/P155" name="wdt:P155"/>
        <!-- followed by (other event) -->
        <data source="http://www.wikidata.org/prop/direct/P156" name="wdt:P156"/>
        <!-- part of the series -->
        <data source="http://www.wikidata.org/prop/direct/P179" name="wdt:P179"/>
        <!-- location (of event) -->
        <data source="http://www.wikidata.org/prop/direct/P276" name="wdt:P276"/>
        <!-- part of -->
        <data source="http://www.wikidata.org/prop/direct/P361" name="wdt:P361"/>
        <!-- Commons category -->
        <data source="http://www.wikidata.org/prop/direct/P373" name="wdt:P373"/>
        <!-- edition number (of event) -->
        <data source="http://www.wikidata.org/prop/direct/P393" name="wdt:P393"/>
        <!-- has part (inverse of 'part of') -->
        <data source="http://www.wikidata.org/prop/direct/P527" name="wdt:P527"/>
        <!-- inception (date or point in time when this event was founded) -->
        <call-macro name="remove-datatype" predicate="http://www.wikidata.org/prop/direct/P571" abbrev="wdt:P571"/>
        <!-- start time (when this event took place) -->
        <call-macro name="remove-datatype" predicate="http://www.wikidata.org/prop/direct/P580" abbrev="wdt:P580"/>
        <!-- end time (when this event took place) -->
        <call-macro name="remove-datatype" predicate="http://www.wikidata.org/prop/direct/P582" abbrev="wdt:P582"/>
        <!-- point of time (when this event took place) -->
        <call-macro name="remove-datatype" predicate="http://www.wikidata.org/prop/direct/P585" abbrev="wdt:P585"/>
        <!-- coordinate location -->
        <call-macro name="remove-datatype" predicate="http://www.wikidata.org/prop/direct/P625" abbrev="wdt:P625"/>
        <!-- organizer -->
        <data source="http://www.wikidata.org/prop/direct/P664" name="wdt:P664"/>
        <!-- participant -->
        <data source="http://www.wikidata.org/prop/direct/P710" name="wdt:P710"/>
        <!-- official website -->
        <data source="http://www.wikidata.org/prop/direct/P856" name="wdt:P856"/>
        <!-- described at URL -->
        <data source="http://www.wikidata.org/prop/direct/P973" name="wdt:P973"/>
        <!-- number of participants-->
        <call-macro name="remove-datatype" predicate="http://www.wikidata.org/prop/direct/P1132" abbrev="wdt:P1132"/>
        <!-- time period -->
        <data source="http://www.wikidata.org/prop/direct/P2348" name="wdt:P2348"/>
        <!-- VIAF ID -->
        <data source="http://www.wikidata.org/prop/direct/P214" name="wdt:P214"/>
        <!-- VIAF URI -->
        <data source="http://www.wikidata.org/prop/direct-normalized/P214" name="wdtn:P214"/>
        <data source="http://www.wikidata.org/prop/direct-normalized/P214" name="owl:sameAs"/>
        <!-- GND ID -->
        <data source="http://www.wikidata.org/prop/direct/P227" name="wdt:P227"/>
        <!-- GND URI -->
        <data source="http://www.wikidata.org/prop/direct-normalized/P227" name="wdtn:P227"/>
        <data source="http://www.wikidata.org/prop/direct-normalized/P227" name="owl:sameAs"/>
        <!-- LoC ID -->
        <data source="http://www.wikidata.org/prop/direct/P244" name="wdt:P244"/>
        <!-- LoC URI -->
        <data source="http://www.wikidata.org/prop/direct-normalized/P244" name="wdtn:P244"/>
        <data source="http://www.wikidata.org/prop/direct-normalized/P244" name="owl:sameAs"/>

        <!-- rdfs:label -->
        <call-macro name="filter-language-tag" predicate="http://www.w3.org/2000/01/rdf-schema#label" tempName="@rdfsLabelFiltered"/>
        <call-macro name="language-tag" predicate="@rdfsLabelFiltered" abbrev="rdfs:label"/>
        <!-- skos:prefLabel-->
        <call-macro name="filter-language-tag" predicate="http://www.w3.org/2004/02/skos/core#prefLabel" tempName="@prefLabelFiltered"/>
        <call-macro name="language-tag" predicate="@prefLabelFiltered" abbrev="skos:prefLabel"/>
        <!-- skos:altLabel -->
        <call-macro name="filter-language-tag" predicate="http://www.w3.org/2004/02/skos/core#altLabel" tempName="@altLabelFiltered"/>
        <call-macro name="language-tag" predicate="@altLabelFiltered" abbrev="skos:altLabel"/>
        <!-- schema:name -->
        <call-macro name="filter-language-tag" predicate="http://schema.org/name" tempName="@schemaNameFiltered"/>
        <call-macro name="language-tag" predicate="@schemaNameFiltered" abbrev="schema:name"/>
        <!-- schema:description -->
        <call-macro name="filter-language-tag" predicate="http://schema.org/description" tempName="@schemaDescriptionFiltered"/>
        <call-macro name="language-tag" predicate="@schemaDescriptionFiltered" abbrev="schema:description"/>
    </rules>

</metamorph>