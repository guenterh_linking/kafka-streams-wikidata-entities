#!/usr/bin/env bash

#can't put it into one cm because amount of data is too big
kubectl create cm cm-wikidata-entities-utility-data-events --from-file=../transformations/events.csv
kubectl create cm cm-wikidata-entities-utility-data-organisations --from-file=../transformations/organisations.csv