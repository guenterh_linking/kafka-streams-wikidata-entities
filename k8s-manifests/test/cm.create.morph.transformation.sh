#!/usr/bin/env bash

kubectl create cm cm-wikidata-entities-morphs --from-file=../transformations/eventsMorph.xml --from-file=../transformations/humanMorph.xml --from-file=../transformations/itemsMorph.xml --from-file=../transformations/organisationMorph.xml